

// for (let i = 10; i > 0; i--) {
//   console.log(i)
// }


/* ---------- Funciones ---------*/

// es o son bloques de codigo rehutilizable , que nos ayudan a mejorar nuestro codigo y hacerlo mas optimo .

// cada funcion si es que llega  a devolver algo se usa return. despues de un return no se pone nada mas


// /* ---------- Suma ---------*/
// function sumaNumeros(a, b) {
//   return a + b
// }


// /* ---------- Resta ---------*/
// function restaNumeros(a, b) {
//   return a - b
// }

// /* ---------- Multiplicacion ---------*/
// function multiplicarNumeros(a, b) {
//   return a * b
// }

// /* ---------- Division ---------*/
// function dividirNumeros(a, b) {
//   return a / b
// }

// console.log(restaNumeros(10, 10))
// console.log(multiplicarNumeros(10, 10))
// console.log(sumaNumeros(10, 10))
// console.log(dividirNumeros(10, 10))


/* ---------- Arrow functions ---------*/

// const sumaNumeros = (a,b) => {
//   return a + b
// }
// const restaNumeros = (a,b) => {
//   return a - b
// }
// const multiplicarNumeros = (a,b) => {
//   return a * b
// }
// const dividirNumeros = (a,b) => {
//   return a / b
// }

// const sumaNumeros = (a,b) => a + b
// const restaNumeros = (a,b) => a - b
// const multiplicarNumeros = (a,b) => a * b
// const dividirNumeros = (a,b) => a / b

// console.log(restaNumeros(10, 10))
// console.log(multiplicarNumeros(10, 10))
// console.log(sumaNumeros(10, 10))
// console.log(dividirNumeros(10, 10))


/* ---------- Ejercicio ---------*/


// pido al usario que ingrese un numero
// valido si es un numero y si es



// function input(){
//   let input = prompt('Ingresa el numero')
//  return input
// }

// function numalcuadrado(){
//   input()
//   if(typeof(input()) === 'number'){
//     return console.log(`${num * num}`)
//   }else{
//     input()
//   }

// }

// let input = prompt('Ingresa el numero')

// console.log(typeof(input))





/* ---------- Ultima clase del primer modulo ---------*/



// const SumaCuadrados = () => {

//   let a = parseInt(prompt('Ingresa el primer numero'),10)
//   let b = parseInt(prompt('Ingresa el segundo numero'),10)

//   resultado = (a * a ) + (b * b)

//   return resultado

// }

// alert(`La suma de los cuadrados es : ${SumaCuadrados()}`)


/* ---------- dos funciones una que suma dos numeros y otra que multiplique dos numeros ---------*/


// const Suma = (num1,num2) => num1 + num2
// const Multiplicar = (num1,num2) => num1 * num2

// console.log(Multiplicar(5,Suma(1,4)))



/* ---------- El area del circulo ---------*/

// const AreaCirculo = (radio) => {
//   const pi = 3.14
//   let Area = pi * (radio * radio)
//   return Area
// }

// console.log(AreaCirculo(5))


/* ---------- Ejercicio de Scope y Hoisting ---------*/

// const Suma = (a,b,c) => a + b + c
// const Resta = (a,b,c) => a - b - c
// const Multiplicacion = (a,b,c) => a * b * c

/* ---------- CallBack : es una funcion pasada como argumento de otra funcion ---------*/

// console.log(Multiplicacion(10,Suma(1,2,3),Resta(5,4,3)))

// const PrimerNombre = (a) => a
// const SegundoNombre = (a) => a
// const PrimerApellido = (a) => a
// const SegundoApellido = (a) => a
// const NombreCompleto = (a,b,c,d) => `Tu nombre completo es : ${a} ${b} ${c} ${d}`

// alert(NombreCompleto(PrimerNombre('Julio'),SegundoNombre('Cesar'),PrimerApellido('Rivera'),SegundoApellido('Pacaya')))



// const num1 = a => a
// const num2 = a => a
// const num3 = a => a
// const Operacion = ( a , b , c ) => (a * b) + Math.pow(c,2)

// console.log( Operacion(num1(5),num2(2),num3(2)) )



// let nombre = 'Diego'

// function llamar(){

//   let nombre = 'DiegoOn'
//   alert(nombre)

// }

// llamar()


/* ---------- Clousure con Funciones normales ---------*/

// function SumNum(a,b,c){

//   function num1(a){
//     return a
//   }

//   function num2(a){
//     return a
//   }

//   function num3(a){
//     return a
//   }

//   return num1(a) + num2(b) + num3(c)
// }


/* ---------- Clousure con Arrow Functions  ---------*/

//  const SunNum = (a,b,c) => {

//   const num1 = a => a
//   const num2 = a => a
//   const num3 = a => a

//   return num1(a) + num2(b) + num3(c)
// }
//  console.log(SunNum(1,2,3))


/* ---------- Arrays ( Tarea ) ---------*/

// let arrayMixto = [1,2,3,4,5,6,'diego','chris','cesar']
// let Suma = 0
// for (let i = 0; i < arrayMixto.length; i++) {

//   if(typeof(arrayMixto[i]) === 'number'){
//     Suma+=arrayMixto[i]
//   }

// }
// console.log(Suma)


/* ---------- Arrays ---------*/

// let Numeros = [1,2,3,4,5,6,7,8,9,10]
// let suma = 0

// for (let i = 0; i < Numeros.length; i++) {
//   suma += Numeros[i]
// }

// console.log(suma)

// let arr = [1,2,3,4]

// let input = prompt('ingresa dato')
/* ---------- Push Agrega Datos al final del array ---------*/
// arr.push(input)

/* ---------- pop() elimina el ultimo dato ---------*/
// arr.pop()
/* ---------- unshift() agrega datos al inicio del array ---------*/

// arr.unshift(10)
/* ---------- shift() elimina el primer dato ---------*/
// arr.shift()
/* ---------- reverse() invierte los indices de un array ---------*/
// arr.reverse()

/* ---------- Concat para unir arrays ---------*/

// let arr1 = [1,2,3]
// let arr2 = [4,5,6]
// let arr3 = arr1.concat(arr2)

/* ---------- map  > recorre todo el array y recibe un callback obligatoriamente ---------*/

// let arr = [1,2,3,4]
// let arrM = arr.map(item => item)
// console.log(arrM)

/* ---------- ejercicio con map sumar 10 y multiplicar por su cuadrado ---------*/

// let arr = [1,2,3,4,5]
// let suma = 0
// let arrEjercicio = arr.map(item => {

//   return suma += (item + 10 ) * (item * item)

// })

/* ----------  Filter > para filtrar arrays ---------*/
// let arr = [1,2,3,4,5,6,7,8,9,10]

// let arrF = arr.filter( i => i > 6)


/* ---------- Reduce > reducir todos los elementos de un array a un unico valor , necesita dos parametros obligatoriamente en el callback , ya que trabaja en pares ---------*/

// let arr = [1,2,3,4,5,6,7,8,9,10]
// let arrR = arr.reduce((a,b) => a+b)



// console.log(arrR)
