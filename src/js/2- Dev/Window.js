/* ---------- window.innerWidth : te da el ancho del viewport ayuda para hacer responsive web design ---------*/

// console.log(window.innerWidth)

/* ---------- scrollY , scrollX : la posicion del scroll en Y y X ---------*/

// console.log(window.scrollY)


/* ---------- window.open('url'): abre una URL como un enlace ---------*/
// console.log(window.open('www.google.com.pe'))

/* ---------- window.history : el historial del navegador  y con el .go(cantidad de paginas) o el .back(cantidad de paginas) para avanzar o retroceder paginas---------*/
// console.log(window.history.go(1))

/* ---------- window.navigator : te da las posiciones del navegador y la localizacion ---------*/



// window.navigator.geolocation.getCurrentPosition(position => {
//   let coor = position.coords;
//   let latitud = coor.latitude;
//   let longitud = coor.longitude;

//   console.log(`${latitud} - ${longitud}`)
// })

/* ---------- window.location: te da la url ---------*/

// console.log(window.location)



// let header = document.getElementById('header')

// window.addEventListener('scroll',()=>{
//   if(window.scrollY > 300){
//     header.classList.add('is-active')
//   }else{
//     header.classList.remove('is-active')
//   }
// })




